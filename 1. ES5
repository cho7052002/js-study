ES5
    https://www.w3schools.com/js/js_es5.asp
    http://speakingjs.com/es5/ch25.html
    2009년 릴리즈 되었다.

    strict 모드 지원
    배열에 forEach, map, filter, reduce, some, every 지원
    Object 형에 대한 getter/setter 지원
    json 지원

1. strict 모드
    안전하고, 경고 적고, 깔끔한 로직으로 자바스크립트를 짜기 위한 모드.
    반대로 일반모드는 sloppy mode 라고 한다.

    다음 위치에 'use strict' 라고 써넣으면 된다.
        JS 파일이나 <script>의 첫 번째 줄
        함수의 첫 번째 줄, 이 때엔 해당 함수만 strict 모드가 된다.
    
    주의할 점
        기존 코드에 strict 모드를 적용하면 동작하지 않을 수도 있다.
        다른 sloppy 코드를 붙여넣거나 잘라내면 동작하지 않을 수도 있다.
    
    strict 모드 기능
        1. 변수를 쓰려면 선언해야 한다
            다음 코드는 동작하는 코드이다.

                function sloppyFunction() {
                    aa = 123;
                }
                sloppyFunction();   //글로별 변수 aa 가 만들어짐
                console.log(aa);    //출력: 123

            하지만 strict 모드에서는 동작하지 않는다.

                function sloppyFunction() {
                    'use strict';
                    aa = 123;
                }
                //aa is not defined.

        2. 함수는 코드의 최상위 스코프에서 선언되어야 한다.
            strict 모드에서는 모든 함수는 최상위 스코프에서 선언되거나, 선언과 동시에 변수에 할당되어야 한다.
            아니면 바로 SyntaxError 가 발생한다.

            다음 코드는 최상위 스코프에서 선언되지 않아 에러가 발생한다.
                function foo() {
                    'use strict'
                    function faa() {...}    //syntax error
                }

            다음 코드는 최상위 스코프에서 선언되지 않았지만 선언과 동시에 변수에 할당해 에러가 발생하지 않는다.
                function foo() {
                    'use strict'
                    var faa = function () {...}
                }

        3. 함수에 넣을 파라미터는 동일한 변수명을 2번 쓰거나, 파라미터 이름과 로컬변수의 이름이 같으면 안된다.

        4. 